# Ministudio Category Converter

## Description
Custom tool for converting categories from old structure (ministudio portals) to new n-n structure

## Install

Edit composer.json file, add in psr-4 object
```

"repositories": [
    {
        "type": "vcs",
        "url": "https://simke94@bitbucket.org/simke94/ministudio-category-converter.git"
    }
],

```

Require this package with composer

```
composer require ministudio/category-converter --dev
```

 Add the ServiceProvider to the providers array in config/app.php
```

'providers' => [

    ...
    
    /*
     * Package Service Providers...
     */
     Ministudio\CategoryConverter\CategoryConverterServiceProvider::class,

    ...

];

```
## Config
Define config for mysql2 connection if not defined
```
'mysql2' => [
    'driver' => env('DB_CONNECTION_SECOND', 'mysql'),
    'url' => env('DATABASE_URL'),
    'host' => env('DB_HOST_SECOND', '127.0.0.1'),
    'port' => env('DB_PORT_PORT', '3306'),
    'database' => env('DB_DATABASE_SECOND', 'forge'),
    'username' => env('DB_USERNAME_SECOND', 'forge'),
    'password' => env('DB_PASSWORD_SECOND', ''),
    'unix_socket' => env('DB_SOCKET_SECOND', ''),
    'charset' => 'latin1',
    'collation' => 'latin1_swedish_ci',
    'prefix' => '',
    'strict' => true,
    'engine' => null,
    'options' => extension_loaded('pdo_mysql') ? array_filter([
        PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
    ]) : [],
],
```

## Usage
- post_table_old - post table name from old db
- post_table_new - post table name from new db
- category_table_new - category table name from new db
- category_post_pivot_new - pivot category_post table from new db
- category_column_name_on_post_old - category column name on old post table (which will be used as reference)
```
convert:categories {--post_table_old=} {--post_table_new=} {--category_table_new=} {--category_post_pivot_new=} {--category_column_name_on_post_old=}


// example
convert:categories --post_table_old=ae_page --post_table_new=posts --category_table_new=categories --category_post_pivot_new=category_post --category_column_name_on_post_old=cat
```

## Warning
On production, DO NOT load CategoryConverterServiceProvider since package will not be installed if u added --dev flag...

