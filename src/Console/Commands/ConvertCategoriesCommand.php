<?php


namespace Ministudio\CategoryConverter\Command;

use Illuminate\Console\Command;
use Ministudio\CategoryConverter\CategoryConverter;

class ConvertCategoriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:categories {--post_table_old=} {--post_table_new=} {--category_table_new=} {--category_column_name_on_post_old=} {--category_post_pivot_new=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert categories from old database to new.';

    /**
     * @var CategoryConverter
     */
    protected $categoryConverter;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->categoryConverter = new CategoryConverter(
            \DB::connection('mysql2'),
            \DB::connection('mysql'),
            $this
        );
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->categoryConverter
            ->setPostTableOld($this->option('post_table_old'))
            ->setPostTableNew($this->option('post_table_new'))
            ->setCategoryTableNew($this->option('category_table_new'))
            ->setCategoryPostPivotNew($this->option('category_post_pivot_new'))
            ->setCategoryColumnName($this->option('category_column_name_on_post_old'))
            ->convert();
    }
}