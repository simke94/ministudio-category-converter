<?php

namespace Ministudio\CategoryConverter;


use Illuminate\Database\ConnectionInterface;
use Ministudio\CategoryConverter\Command\ConvertCategoriesCommand;

class CategoryConverter
{
    public $chunkRecords = 600;
    public $db_source;
    public $db_new;
    protected $commandInstance;
    protected $postTableNameOld;
    protected $postTableNameNew;
    protected $categoryColumnName;
    protected $categoryPostPivotTableName;
    private $categoryTableNameNew;

    /**
     * CategoryConverter constructor.
     *
     * @param ConnectionInterface $db_source
     * @param ConnectionInterface $db_new
     * @param ConvertCategoriesCommand $commandInstance
     * @param string $postTableNameOld
     * @param string $categoryTableNameNew
     * @param string $categoryPostPivotTableNameNew
     */
    public function __construct(
        ConnectionInterface $db_source,
        ConnectionInterface $db_new,
        ConvertCategoriesCommand $commandInstance
    )
    {
        $this->db_source = $db_source;
        $this->db_new = $db_new;
        $this->commandInstance = $commandInstance;
    }
    
    public function setPostTableOld(string $name = 'old_page')
    {
        $this->postTableNameOld = $name;

        return $this;
    }
    
    public function setPostTableNew(string $name = 'posts')
    {
        $this->postTableNameNew = $name;

        return $this;
    }

    public function setCategoryTableNew(string $name = 'categories')
    {
        $this->categoryTableNameNew = $name;

        return $this;
    }

    public function setCategoryPostPivotNew(string $name = 'category_post')
    {
        $this->categoryPostPivotTableName = $name;

        return $this;
    }
    
    public function setCategoryColumnName(string $name = 'cat')
    {
        $this->categoryColumnName = $name;

        return $this;
    }

    public function convert()
    {
        $categoryColumnName = $this->categoryColumnName;

        $posts = $this->db_source
            ->table($this->postTableNameOld)
            ->select('*')
            ->orderBy('id')
            ->chunk($this->chunkRecords, function ($posts) use ($categoryColumnName) {

                // Define variable as array
                $inserts = array();

                // For each record from source table, rename source column names
                foreach ($posts as $row => $post) {

                    //dd($post->cat);

                    $categoriesIds = [];

                    $category = $this->db_new
                        ->table($this->categoryTableNameNew)
                        ->where('id', $post->$categoryColumnName)->first();

                    $postFromNew = $this->db_new
                        ->table($this->postTableNameNew)
                        ->where('id', $post->id)->first();

                    if ($category && $postFromNew) {
                        if (isset($category->parent_id) && $category->parent_id) {

                            $categoriesIds[] = $category->parent_id;
                        }

                        $categoriesIds[] = $category->id;

                        foreach ($categoriesIds as $catId) {

                            $inserts[] = ['category_id' => $catId, 'post_id' => $post->id];
                        }
                    }
                }

                $this->db_new->table($this->categoryPostPivotTableName)->insert($inserts);

            });
    }


}