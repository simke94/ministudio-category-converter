<?php

namespace Ministudio\CategoryConverter;


use Illuminate\Support\ServiceProvider;
use Ministudio\CategoryConverter\Command\ConvertCategoriesCommand;

class CategoryConverterServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands(ConvertCategoriesCommand::class);
        }
    }
}